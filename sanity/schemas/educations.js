export default {
    name: 'educations',
    title: 'Educations',
    type: 'document',
    fields: [
        {
            name: 'image',
            title: 'Image',
            type: 'image',
            options: {
                hotspot: true,
            },
        },
        {
            name: 'subject',
            title: 'Subject',
            type: 'string',
        },
        {
            name: 'institute',
            title: 'Institute',
            type: 'string',
        },
        {
            name: 'type',
            title: 'Education Type',
            type: 'string',
        },
        {
            name: 'link',
            title: 'Certificate Link',
            type: 'string',
        },
        {
            name: 'start',
            title: 'Start Date',
            type: 'date',
        },
        {
            name: 'end',
            title: 'End Date',
            type: 'date',
        },
    ],
};
