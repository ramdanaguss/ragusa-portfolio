// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator';

// Then import schema types from any plugins that might expose them
import schemaTypes from 'all:part:@sanity/base/schema-type';

// We import object and document schemas
import blockContent from './blockContent';
import pageInfo from './pageInfo';
import skills from './skills';
import projects from './projects';
import educations from './educations';
import contact from './contact';

export default createSchema({
    name: 'ragusa-portfolio',
    types: schemaTypes.concat([
        pageInfo,
        skills,
        projects,
        educations,
        contact,
        blockContent,
    ]),
});
