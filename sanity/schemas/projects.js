export default {
    name: 'projects',
    title: 'Projects',
    type: 'document',
    fields: [
        {
            name: 'image',
            title: 'Image',
            type: 'image',
        },
        {
            name: 'techStack',
            title: 'Tech Stack',
            type: 'array',
            of: [{ type: 'reference', to: [{ type: 'skills' }] }],
        },
        {
            name: 'title',
            title: 'Title',
            type: 'string',
        },
        {
            name: 'description',
            title: 'Project Description',
            type: 'text',
        },
        {
            name: 'liveServer',
            title: 'Live Server Link',
            type: 'string',
        },
        {
            name: 'repository',
            title: 'Repository Link',
            type: 'string',
        },
    ],
};
