export default {
    name: 'pageInfo',
    title: 'PageInfo',
    type: 'document',
    fields: [
        {
            name: 'profileImage',
            title: 'Profile Image',
            type: 'image',
            options: {
                hotspot: true,
            },
        },
        {
            name: 'social',
            title: 'Social Link',
            type: 'array',
            of: [{ type: 'string' }],
        },
        {
            name: 'cv',
            title: 'CV Link',
            type: 'string',
        },
        {
            name: 'typewriter',
            title: 'Typewriter',
            type: 'array',
            of: [{ type: 'string' }],
        },
        {
            name: 'background',
            title: 'Background',
            type: 'text',
        },
        {
            name: 'backgroundImage',
            title: 'Background Image',
            type: 'image',
            options: {
                hotspot: true,
            },
        },
        {
            name: 'skills',
            title: 'Skills',
            type: 'array',
            of: [{ type: 'reference', to: [{ type: 'skills' }] }],
        },
        {
            name: 'projects',
            title: 'Projects',
            type: 'array',
            of: [{ type: 'reference', to: [{ type: 'projects' }] }],
        },
        {
            name: 'educations',
            title: 'Educations',
            type: 'array',
            of: [{ type: 'reference', to: [{ type: 'educations' }] }],
        },
        {
            name: 'contact',
            title: 'Contact',
            type: 'reference',
            to: [{ type: 'contact' }],
        },
    ],
};
