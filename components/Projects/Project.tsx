import Image from 'next/image';
import Link from 'next/link';
import { motion } from 'framer-motion';
import { Project, Skill } from '../../sanity-query';
import { urlFor } from '../../libs/sanity-img-builder';

type Props = {
    project: Project;
    projectTotal: number;
    projectNumber: number;
    techStack: Skill[];
};

function Project({ project, projectTotal, projectNumber, techStack }: Props) {
    const ts = ['NextJs', 'NodeJs', 'Bootstrap', 'Tailwind', 'MongoDB'];
    return (
        <div className="flex w-screen flex-shrink-0 snap-center flex-col items-center justify-center gap-y-5">
            <motion.div
                initial={{ scale: 0 }}
                whileInView={{
                    scale: [0.5, 1.2, 1],
                    rotate: [-180, 360],
                }}
                viewport={{ once: true }}
                transition={{ duration: 1.3 }}
                className="relative  h-40 w-[90vw] xxs:h-56 xs:h-60 xs:max-w-md"
            >
                <Image
                    src={urlFor(project.image).url()}
                    alt={project.title}
                    className="rounded-md border  border-white object-contain p-4"
                    sizes="(min-width: 349px) 90vw, (min-width: 400px) 224px, (min-width: 500px) 512px"
                    fill
                />
            </motion.div>

            <div className="flex flex-col items-center gap-y-3 px-3 xs:max-w-lg">
                <h4 className="text-center text-xl">
                    Project {projectNumber} of {projectTotal}:{' '}
                    <span className="underline decoration-[#F7AB0A] decoration-wavy">
                        {project.title}
                    </span>
                </h4>

                <div className="flex flex-wrap gap-x-2 gap-y-2">
                    {techStack.map((ts) => (
                        <span
                            key={ts._id}
                            className="rounded-full border border-[#F7AB0A] py-1 px-2 text-xxs xs:text-xs"
                        >
                            {ts.name}
                        </span>
                    ))}
                </div>

                <div className="space-y-2 px-2 text-left sm:space-y-3">
                    <p className="text-xs md:text-sm">{project.description}</p>

                    <div className="flex flex-col space-y-1 text-[#F7AB0A] underline decoration-white decoration-wavy">
                        <Link href={project.liveServer} target="_blank">
                            Live Demo
                        </Link>

                        <Link href={project.repository} target="_blank">
                            Repository
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Project;
