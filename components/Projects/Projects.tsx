import { motion } from 'framer-motion';
import Project from './Project';
import { Project as ProjectType } from '../../sanity-query';

type Props = {
    projects: ProjectType[];
};

function Projects({ projects }: Props) {
    return (
        <motion.div
            initial={{ opacity: 0 }}
            whileInView={{ opacity: 1 }}
            viewport={{ once: true }}
            transition={{ duration: 0.7 }}
            className="relative flex h-screen flex-col items-center justify-center"
        >
            <h2 className="header-section">Projects</h2>

            <div className="relative z-20 mt-5 flex h-screen w-full snap-x snap-mandatory items-center  overflow-x-scroll scrollbar-thin scrollbar-thumb-[#F7AB0A] md:mt-24">
                {projects.map((project, i) => (
                    <Project
                        key={i}
                        project={project}
                        projectNumber={i + 1}
                        projectTotal={projects.length}
                        techStack={project.techStack}
                    />
                ))}
            </div>

            <div className="absolute top-[25%] z-0 h-[400px] w-full -skew-y-12 bg-[#F7AB0A]/10" />
        </motion.div>
    );
}

export default Projects;
