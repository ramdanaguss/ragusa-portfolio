import Link from 'next/link';
import { SocialIcon } from 'react-social-icons';
import { DocumentTextIcon } from '@heroicons/react/24/solid';
import { motion } from 'framer-motion';
import { useEffect, useState } from 'react';
import { Core as CoreType } from '../../sanity-query';

type Props = {
    core: CoreType;
};

function Header({ core }: Props) {
    // Setup window object
    const [windowObj, setWindowObj] = useState<Window>();
    useEffect(() => {
        setWindowObj(window);
    }, []);

    return (
        <header className="sticky top-0 z-50 mx-auto flex max-w-7xl items-center justify-between bg-transparent p-3 pr-8  sm:p-5">
            <motion.div
                initial={{
                    x: -500,
                    scale: 0.5,
                    opacity: 0,
                }}
                animate={{
                    x: 0,
                    opacity: 1,
                    scale: 1,
                }}
                transition={{ duration: 1.5 }}
            >
                {core.social.map((item) => (
                    <SocialIcon
                        key={item}
                        url={item}
                        bgColor="transparent"
                        fgColor="gray"
                        target="_blank"
                    />
                ))}

                <SocialIcon
                    url={`${windowObj?.location.origin}#contact-me`}
                    network="email"
                    bgColor="transparent"
                    fgColor="gray"
                />
            </motion.div>

            <motion.div
                initial={{
                    x: 500,
                    scale: 0.5,
                    opacity: 0,
                }}
                animate={{
                    x: 0,
                    opacity: 1,
                    scale: 1,
                }}
                transition={{ duration: 1.5 }}
                className="flex cursor-pointer  items-center text-gray-300"
            >
                <div className=" items-center ">
                    <Link
                        href={core.cv}
                        target="_blank"
                        className=" flex gap-x-1 uppercase text-gray-400"
                    >
                        <DocumentTextIcon className="h-6 w-6" />
                        CV
                    </Link>
                </div>
            </motion.div>
        </header>
    );
}

export default Header;
