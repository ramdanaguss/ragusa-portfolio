import React from 'react';
import Skill from './Skill';
import { Skill as SkillType } from '../../sanity-query';

type Props = {
    skills: SkillType[];
};

function Skills({ skills }: Props) {
    // Setup animation
    skills.forEach((skill, i) => {
        if (i % 2 === 0) return (skill.animation = true);
        skill.animation = false;
    });

    return (
        <div className="relative flex h-screen flex-col items-center justify-center">
            <h2 className="header-section">Skills</h2>

            <div className="mt-10 grid grid-cols-4 gap-5 p-4">
                {skills.map((skill) => (
                    <Skill
                        key={skill._id}
                        upDirection={skill.animation as boolean}
                        image={skill.image}
                    />
                ))}
            </div>
        </div>
    );
}

export default Skills;
