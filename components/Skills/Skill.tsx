import Image from 'next/image';
import { motion } from 'framer-motion';
import { SanityImage } from '../../sanity-query';
import { urlFor } from '../../libs/sanity-img-builder';

type Props = {
    upDirection: boolean;
    image: SanityImage;
};

function Skill({ upDirection, image }: Props) {
    return (
        <motion.div
            initial={{ y: upDirection ? -150 : 150, opacity: 0, rotate: 0 }}
            whileInView={{ y: 0, opacity: 1, rotate: [180, -180, 0] }}
            whileHover={{ rotate: [180, -180, 0], scale: 1.1 }}
            viewport={{ once: true }}
            transition={{ duration: 1.3 }}
            className="relative h-16 w-16 overflow-hidden rounded-full border border-gray-500 sm:h-20 sm:w-20"
        >
            <Image
                src={urlFor(image).url()}
                alt="Skill logo"
                className="bg-white object-contain p-2"
                sizes="(min-width: 349px) 64px, (min-width: 640px) 80px, (min-width: 1024px) 96px"
                fill
            />
        </motion.div>
    );
}

export default Skill;
