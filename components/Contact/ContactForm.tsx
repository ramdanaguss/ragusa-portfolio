import { useForm, SubmitHandler } from 'react-hook-form';
import { motion } from 'framer-motion';

type Props = {};

type Input = {
    name: string;
    email: string;
    subject: string;
    message: string;
};

function ContactForm({}: Props) {
    const {
        register,
        formState: { errors },
        handleSubmit,
    } = useForm({
        defaultValues: {
            name: '',
            email: '',
            subject: '',
            message: '',
        },
    });

    const onSubmit: SubmitHandler<Input> = ({ message, name, email, subject }) => {
        window.location.href = `mailto:ramdanaguss@gmail?subject=${subject}&body=Hi, my name is ${name}. ${message} (${email})`;
    };

    return (
        <form
            onSubmit={handleSubmit(onSubmit)}
            className="mx-auto w-64 sm:w-[70vw] md:w-[500px]"
        >
            <div className="flex flex-col items-center justify-center gap-y-2">
                <div className="flex w-full flex-col gap-y-2 sm:flex-row sm:gap-x-2">
                    <motion.div
                        initial={{ x: -200, opacity: 0 }}
                        whileInView={{ x: 0, opacity: 1 }}
                        viewport={{ once: true }}
                        transition={{ duration: 1, delay: 1.1 }}
                        className="w-full"
                    >
                        <input
                            className="input"
                            type="text"
                            placeholder="Name"
                            {...register('name', {
                                required: 'Please provide a name',
                            })}
                        />
                        <p className="input-error">{errors?.name?.message}</p>
                    </motion.div>

                    <motion.div
                        initial={{ x: 200, opacity: 0 }}
                        whileInView={{ x: 0, opacity: 1 }}
                        viewport={{ once: true }}
                        transition={{ duration: 1, delay: 1.1 }}
                        className="w-full"
                    >
                        <input
                            className="input"
                            type="email"
                            placeholder="Email"
                            {...register('email', {
                                required: 'Please provide an email address',
                                pattern: {
                                    value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                    message: 'Please provide a valid email address',
                                },
                            })}
                        />
                        <p className="input-error">{errors?.email?.message}</p>
                    </motion.div>
                </div>

                <motion.div
                    initial={{ y: 150, opacity: 0 }}
                    whileInView={{ y: 0, opacity: 1 }}
                    viewport={{ once: true }}
                    transition={{ duration: 1, delay: 1.3 }}
                    className="w-full"
                >
                    <input
                        className="input"
                        type="text"
                        placeholder="Subject"
                        {...register('subject', {
                            required: 'Please provide a subject',
                        })}
                    />
                    <p hidden={!errors} className="input-error">
                        {errors?.subject?.message}
                    </p>
                </motion.div>

                <motion.textarea
                    initial={{ y: 150, opacity: 0 }}
                    whileInView={{ y: 0, opacity: 1 }}
                    viewport={{ once: true }}
                    transition={{ duration: 1, delay: 1.4 }}
                    className="input"
                    {...register('message')}
                    placeholder="Message"
                    rows={4}
                />

                <motion.button
                    initial={{ y: 50, opacity: 0 }}
                    whileInView={{ y: 0, opacity: 1 }}
                    viewport={{ once: true }}
                    transition={{ duration: 1, delay: 1.5 }}
                    className="w-full rounded-md bg-[#F7AB0A] px-1 py-2 font-bold text-black sm:px-2 sm:py-4"
                >
                    Send
                </motion.button>
            </div>
        </form>
    );
}

export default ContactForm;
