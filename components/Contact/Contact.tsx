import { PhoneIcon, EnvelopeIcon, MapPinIcon } from '@heroicons/react/24/solid';
import { motion } from 'framer-motion';
import ContactForm from './ContactForm';
import { Contact } from '../../sanity-query';

type Props = {
    contact: Contact;
};

function Contact({ contact }: Props) {
    return (
        <motion.div
            initial={{ opacity: 0 }}
            whileInView={{ opacity: 1 }}
            viewport={{ once: true }}
            transition={{ duration: 1.3 }}
            className="relative flex h-screen flex-col items-center justify-center"
        >
            <h3 className="header-section">Contact</h3>

            <div className="mt-10 space-y-4 text-center sm:mt-20 md:space-y-7">
                <motion.h4
                    initial={{ y: -100 }}
                    whileInView={{ y: 0 }}
                    viewport={{ once: true }}
                    transition={{ duration: 1 }}
                    className="text-xl uppercase xxs:text-2xl sm:text-3xl"
                >
                    open for <span className="wavy-underline-orange">hiring</span> and{' '}
                    <span className="wavy-underline-orange">collaboration</span>
                </motion.h4>

                <motion.div
                    initial={{ x: -100 }}
                    whileInView={{ x: 0 }}
                    viewport={{ once: true }}
                    transition={{ duration: 1 }}
                    className="contact-info"
                >
                    <PhoneIcon className="h-5 w-5 animate-bounce text-[#F7AB0A]/40" />
                    +62{contact.phone}
                </motion.div>

                <motion.div
                    initial={{ x: 100 }}
                    whileInView={{ x: 0 }}
                    viewport={{ once: true }}
                    transition={{ duration: 1 }}
                    className="contact-info"
                >
                    <EnvelopeIcon className="h-5 w-5  text-[#F7AB0A]/40" />
                    {contact.email}
                </motion.div>

                <motion.div
                    initial={{ x: -100 }}
                    whileInView={{ x: 0 }}
                    viewport={{ once: true }}
                    transition={{ duration: 1 }}
                    className="contact-info"
                >
                    <MapPinIcon className="h-5 w-5  text-[#F7AB0A]/40" />
                    {contact.address}
                </motion.div>

                <ContactForm />
            </div>
        </motion.div>
    );
}

export default Contact;
