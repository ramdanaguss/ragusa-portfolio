import Image from 'next/image';
import { motion } from 'framer-motion';
import { Core as CoreType } from '../../sanity-query';
import { urlFor } from '../../libs/sanity-img-builder';

type Props = {
    core: CoreType;
};

function About({ core }: Props) {
    return (
        <motion.div className="relative mx-auto my-20 flex h-[110vh] max-w-7xl flex-col items-center justify-evenly">
            <h2 className="header-section">About</h2>

            <motion.div
                initial={{ opacity: 0 }}
                whileInView={{ opacity: 1 }}
                transition={{ duration: 1.3 }}
                viewport={{ once: true }}
                className="mt-5 flex flex-col text-center md:flex-row md:items-center md:gap-x-10 md:px-10 lg:gap-x-14 lg:px-20"
            >
                <motion.div
                    initial={{ x: -200 }}
                    whileInView={{ x: 0 }}
                    viewport={{ once: true }}
                    transition={{ duration: 1 }}
                    className="relative mx-auto  mb-6 h-32 w-32 flex-shrink-0 sm:h-56 sm:w-56 md:h-96 md:w-64 lg:h-[448px] lg:w-80 xl:h-[512px] xl:w-96"
                >
                    <Image
                        src={urlFor(core.backgroundImage).url()}
                        alt="About Image"
                        className="rounded-full object-cover md:rounded-lg "
                        sizes="(min-width: 349px) 428px, (min-width: 640px) 524px, (min-width: 768px) 556px, (min-width: 1024px) 620px, (min-width: 1280px) 684px"
                        fill
                    />
                </motion.div>

                <div className="flex flex-col gap-y-7">
                    <h3 className=" text-2xl sm:text-4xl">
                        Here is my{' '}
                        <span className="wavy-underline-orange">background</span>
                    </h3>

                    <p className="px-3 text-sm sm:text-base md:text-left">
                        {core.background}
                    </p>
                </div>
            </motion.div>
        </motion.div>
    );
}

export default About;
