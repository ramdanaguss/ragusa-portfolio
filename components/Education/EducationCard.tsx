import Link from 'next/link';
import Image from 'next/image';
import { motion } from 'framer-motion';
import { Education } from '../../sanity-query';
import { urlFor } from '../../libs/sanity-img-builder';

type Props = {
    education: Education;
    totalEducation: number;
    currentEducation: number;
};

function EducationCard({ education, currentEducation, totalEducation }: Props) {
    // Setup start and end date
    let startDate: string = '';
    if (education.start)
        startDate = new Date(education.start).toLocaleDateString('id-ID', {
            year: 'numeric',
            month: 'long',
        });
    let endDate = new Date(education.end).toLocaleDateString('id-ID', {
        year: 'numeric',
        month: 'long',
    });

    // Setup period
    const period = startDate ? `${startDate} - ${endDate}` : `Completed ${endDate}`;
    return (
        <Link href={education.link} target="_blank">
            <article className="mx-4 my-8 flex h-[468px] w-[400px] flex-shrink-0 cursor-pointer snap-center flex-col bg-[#292929] py-16 opacity-100 duration-100 hover:opacity-100 xs:w-[450px] lg:opacity-40 xl:w-[600px]">
                <motion.div
                    initial={{ y: -100 }}
                    whileInView={{ y: 0 }}
                    viewport={{ once: true }}
                    transition={{ duration: 1 }}
                    className="relative mx-auto mb-8 h-32 w-32  xl:h-36 xl:w-36"
                >
                    <Image
                        src={urlFor(education.image).url()}
                        alt={education.subject}
                        className="rounded-full object-cover"
                        sizes="(min-width: 349px) 328px, (min-width: 1280px) 544px"
                        fill
                    />
                </motion.div>

                <div className="mx-auto px-10">
                    <h3 className="mb-0.5 text-3xl font-light capitalize sm:text-4xl">
                        {education.institute}
                    </h3>

                    <h4 className="mb-5 uppercase tracking-widest text-[#F7AB0A]">
                        {education.type}
                    </h4>

                    <h5 className="mb-1 text-sm font-bold uppercase  tracking-widest sm:text-base">
                        {education.subject}
                    </h5>

                    <p className="mb-5 text-xs capitalize text-[#F7AB0A] sm:text-sm">
                        {period}
                    </p>

                    <p className="text-xs md:hidden">
                        Education {currentEducation} of {totalEducation}
                    </p>
                </div>
            </article>
        </Link>
    );
}

export default EducationCard;
