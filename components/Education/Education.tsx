import { motion } from 'framer-motion';
import EducationCard from './EducationCard';
import { Education } from '../../sanity-query';

type Props = {
    educations: Education[];
};

function Education({ educations }: Props) {
    return (
        <motion.div
            initial={{ opacity: 0 }}
            whileInView={{ opacity: 1 }}
            transition={{ duration: 1.3 }}
            viewport={{ once: true }}
            className="relative flex h-screen max-w-full flex-col items-center justify-evenly overflow-hidden"
        >
            <h2 className="absolute top-[70px] left-1/2 -translate-x-1/2 text-lg uppercase tracking-[10px] text-gray-500 sm:top-20 sm:tracking-[20px] md:text-2xl">
                Education
            </h2>

            <div className="mt-16 flex h-[95vh] w-full snap-x snap-mandatory items-center overflow-x-scroll px-5 scrollbar-thin  scrollbar-thumb-[#F7AB0A] ">
                {educations.map((edu, i) => (
                    <EducationCard
                        key={edu._id}
                        education={edu}
                        currentEducation={i + 1}
                        totalEducation={educations.length}
                    />
                ))}
            </div>
        </motion.div>
    );
}

export default Education;
