import Image from 'next/image';
import Link from 'next/link';
import { useTypewriter, Cursor } from 'react-simple-typewriter';
import CircleBackground from './CircleBackground';
import { Core as CoreType } from '../../sanity-query';
import { urlFor } from '../../libs/sanity-img-builder';

type Props = {
    core: CoreType;
};

function Hero({ core }: Props) {
    // Typewriter
    const [text] = useTypewriter({
        words: core.typewriter,
        delaySpeed: 2000,
        loop: false,
    });

    return (
        <div className="relative z-40 flex h-screen flex-col items-center justify-center  bg-[rgb(36,36,36)] text-center">
            <CircleBackground />
            <div className="z-30 w-full space-y-5 sm:space-y-4">
                <Image
                    src={urlFor(core.profileImage).url()}
                    alt="Profile image"
                    width={256}
                    height={256}
                    className="mx-auto  h-32 w-32 rounded-full md:h-40 md:w-40"
                    priority={true}
                />

                <h2 className="mx-auto  w-max font-bold uppercase tracking-[0.5rem] text-gray-500">
                    Web Developer
                </h2>

                <h1 className=" text-[22px] font-semibold sm:text-4xl md:text-5xl xl:text-6xl">
                    <span>{text}</span>
                    <Cursor cursorColor="#F7AB0A" />
                </h1>

                <div className="mx-auto flex flex-wrap justify-center gap-y-3 gap-x-4  text-gray-500 md:gap-x-6 md:text-lg">
                    <Link href="#about" className="hero-button">
                        About
                    </Link>

                    <Link href="#skills" className="hero-button">
                        Skills
                    </Link>

                    <Link href="#education" className="hero-button">
                        Education
                    </Link>

                    <Link href="#projects" className="hero-button">
                        Projects
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default Hero;
