import { motion } from 'framer-motion';
type Props = {};

function CircleBackground({}: Props) {
    return (
        <motion.div
            animate={{
                scale: [1, 1.5, 1.5, 2.5, 1],
                opacity: [0.1, 0.2, 0.4, 0.8, 0.1, 0.5, 1],
            }}
            transition={{ duration: 1.2 }}
            className="relative top-[9%] flex items-center justify-center md:top-[12%]"
        >
            <div className="absolute z-20 h-[100px]  w-[100px] animate-ping rounded-full border border-[#333] sm:h-[200px] sm:w-[200px] md:h-[250px] md:w-[250px]" />
            <div className="absolute z-20 h-[200px]  w-[200px] rounded-full border border-[#333] sm:h-[300px] sm:w-[300px] md:h-[350px] md:w-[350px]" />
            <div className="absolute z-20 h-[400px]  w-[400px] rounded-full border border-[#333] sm:h-[500px] sm:w-[500px] md:h-[550px] md:w-[550px]" />
            <div className="absolute z-20 h-[580px]  w-[550px] animate-pulse rounded-full border border-[#F7AB0A] opacity-20 sm:h-[650px] sm:w-[650px] md:h-[700px] md:w-[700px]" />
            <div className="absolute z-20 h-[700px]  w-[700px]  rounded-full border border-[#333] sm:h-[800px] sm:w-[800px]" />
        </motion.div>
    );
}

export default CircleBackground;
