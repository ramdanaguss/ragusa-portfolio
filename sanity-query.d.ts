import Education from './components/Education/Education';

interface SanityBody {
    _createdAt: string;
    _id: string;
    _rev: string;
    _updatedAt: string;
}

export interface SanityImage {
    _type: 'image';
    asset: {
        _ref: string;
        _type: string;
    };
}

export interface Contact extends SanityBody {
    _type: 'contact';
    address: string;
    email: string;
    phone: number;
    title: string;
}

export interface Education extends SanityBody {
    _type: 'educations';
    end: string;
    start: string;
    image: SanityImage;
    institute: string;
    link: string;
    subject: string;
    type: string;
}

export interface Project extends SanityBody {
    _type: 'projects';
    image: SanityImage;
    liveServer: string;
    repository: string;
    title: string;
    description: string;
    techStack: Skill[];
}

export interface Skill extends SanityBody {
    _type: 'skills';
    image: SanityImage;
    name: string;
    animation?: boolean;
}

export interface Core extends SanityBody {
    background: string;
    backgroundImage: SanityImage;
    cv: string;
    profileImage: SanityImage;
    social: string[];
    typewriter: string[];
}

export interface PageInfo extends Contact, Education, Project, Skill, Core {
    _type: 'pageInfo';
    contact: Contact;
    educations: Education[];
    skills: Skill[];
    projects: Project[];
}
