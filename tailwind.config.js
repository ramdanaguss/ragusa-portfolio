/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
    theme: {
        extend: {
            screens: {
                xxs: '400px',
                xs: '500px',
            },
            fontSize: {
                xxs: '10px',
            },
        },
    },
    plugins: [require('tailwind-scrollbar')],
};
