import Head from 'next/head';
import About from '../components/About/About';
import Contact from '../components/Contact/Contact';
import Education from '../components/Education/Education';
import Header from '../components/Header/Header';
import Hero from '../components/Hero/Hero';
import Projects from '../components/Projects/Projects';
import Skills from '../components/Skills/Skills';
import { GetStaticProps } from 'next';
import { getPageInfo } from '../libs/sanity-client-config';
import {
    Core as CoreType,
    Project as ProjectType,
    Education as EducationType,
    Skill as SkillType,
    Contact as ContactType,
    PageInfo as PageInfoType,
} from '../sanity-query';

type props = {
    core: CoreType;
    skills: SkillType[];
    projects: ProjectType[];
    educations: EducationType[];
    contact: ContactType;
};

export default function Home({ core, projects, skills, educations, contact }: props) {
    return (
        <div className="h-screen snap-y snap-mandatory overflow-y-scroll bg-[rgb(36,36,36)] text-white overflow-x-hidden scrollbar-thin scrollbar-track-gray-400/30 scrollbar-thumb-[#F7AB0A]">
            <Head>
                <title>Ramdan | Portfolio</title>
                <link rel="shortcut icon" href="/Icon/Ragusa-logo.ico" />
            </Head>

            <Header core={core} />

            <section id="hero" className="snap-start">
                <Hero core={core} />
            </section>

            <section id="about" className="snap-start">
                <About core={core} />
            </section>

            <section id="skills" className="snap-center">
                <Skills skills={skills} />
            </section>

            <section id="projects" className="snap-center">
                <Projects projects={projects} />
            </section>

            <section id="education" className="snap-start">
                <Education educations={educations} />
            </section>

            <section id="contact-me" className="snap-start">
                <Contact contact={contact} />
            </section>
        </div>
    );
}

export const getStaticProps: GetStaticProps = async () => {
    const pageInfo = await getPageInfo();
    const { contact, educations, projects, skills } = pageInfo;

    const core = { ...pageInfo };
    const deletedKey = ['projects', 'skills', 'contact', 'educations'];
    deletedKey.forEach((key: string) => delete core[key as keyof PageInfoType]);

    return {
        props: {
            core,
            skills,
            projects,
            educations,
            contact,
        },
        revalidate: 60,
    };
};
