import { createClient } from 'next-sanity';
import { PageInfo, Skill } from '../sanity-query';

export const client = createClient({
    projectId: 'oddxvobl',
    dataset: 'production',
    apiVersion: '2022-11-25',
    useCdn: process.env.NODE_ENV === 'production',
});

export async function getPageInfo(): Promise<PageInfo> {
    const pageInfoArr = await client.fetch(
        '*[_type == "pageInfo"]{...,contact->,educations[]->,skills[]->,"projects": *[_type == "projects"] {...,techStack[]->}}'
    );

    const [pageInfo]: PageInfo[] = pageInfoArr;

    return pageInfo;
}
