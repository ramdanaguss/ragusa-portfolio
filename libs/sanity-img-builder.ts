import imageUrlBuilder from '@sanity/image-url';
import { SanityImage } from '../sanity-query';
import { client } from './sanity-client-config';

const builder = imageUrlBuilder(client);

export function urlFor(source: SanityImage) {
    return builder.image(source);
}
